# Pokemon Training Tracker Issues Only
This is an Android application (Plan to be on IOS in near future) where you as pokemon trainer can track the EV's, IV's and team composition for casual or competitive.

This is not an open source app and this repository is only for user who wants requests a new feature or report a bug.

This app is designed to be complete free with no intrusive ads, but the idea is have a "support" section where users can show a serie of ads (images and/or videos) and only if this section if user want to support the development.

Whit this app you can:
- Build your pokemon setting up the EVs, IVs, Moveset, item you desire.
- Track the EVs training process of your Pokemon.
- Group your pokemon in teams.

## Want to test?

At this point the application is in internal testing, if you want colavorate to find bugs or add suggestions about functionality you desire, please add a comment with your email in this [ticket](https://gitlab.com/issues-tracker/pokemon-training-tracker/-/issues/1)

## Report an issue or request for a new feature
When you find and issue or want to request a new feature please go to the [issues](https://gitlab.com/issues-tracker/pokemon-training-tracker/-/issues) and please look if an issue or feature similar already exist, if is the case please click on "like" button, the most liked tickets will have priority also, feel free to comment in the ticket if you want to add anything to it.

If a ticket with your request did not exist, please open one following the patterns below.

### Bug
- Title: Very short description of the request.
- Summary:
    - Add an extended description about the issue found.
    - Provide the steps to reproduce.
    - If possible add an screenshot showing the described issue.
- Type: Incident
- Tag the ticket with "bug"

### Feature
- Title: Very short description of the request.
- Summary:
    - Add an extended description about the feature you want to be added into the application.
    - If possible add images.
- Type: Issue
- Tag the ticket with "feature-request".

## How tickets are attend
- Bugs have priority over feature requests.
- In both cases the tickets are attended in orded of most liked to least.
- In case a bug can not be reproducible user will be tag asking for more info, if the user didi not response, the ticket should be closed as "not reproducible".
- Once a ticket is mark to be attended it will be updated with the proposed milestone.

## Disclaimer
This is a only one developer team who maintains the entire application and the idea as commented before is accept the support of the ussers, that's because is not possible hire more developers, taking this on count, resolve tickets can take time but I made my best to work harder and faster I can.

Pokemon Training Tracker use [PokemonAPI](https://pokeapi.co) following the [Fair Use Policy](https://pokeapi.co/docs/v2#fairuse) and the intention is contribute to grow up this incredible tool.

Pokemon Training Tracket is an unofficial, free fan made app and is NOT affiliated, endorsed or supported by Nintendo, GAME FREAK or The Pokémon company in any way.
Some images used in this app are copyrighted and are supported under fair use.
Pokémon and Pokémon character names are trademarks of Nintendo.
No copyright infringement intended.

Pokémon © 2002-2022 Pokémon. © 1995-2022 Nintendo/Creatures Inc./GAME FREAK inc.